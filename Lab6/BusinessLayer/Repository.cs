﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;

namespace BusinessLayer
{
    public class Repository : IRepository
    {
        private readonly POIContext pOIContext;

        public Repository(POIContext pOIContext)
        {
            this.pOIContext = pOIContext;
        }

        public void Add(POI entity)
        {
            //var poi = new POI()
            //{
            //    ID = entity.ID,
            //    Name = entity.Name,
            //    Longitude = entity.Longitude,
            //    Latitude = entity.Latitude
            //};
            pOIContext.POIS.Add(entity);
            pOIContext.SaveChanges();
        }

        public void Delete(POI entity)
        {
            //var poi = new POI()
            //{
            //    ID = entity.ID,
            //    Name = entity.Name,
            //    Longitude = entity.Longitude,
            //    Latitude = entity.Latitude
            //};
            pOIContext.POIS.Remove(entity);
            pOIContext.SaveChanges();
        }

        public IReadOnlyList<POI> GetAll()
        {
            //return pOIContext.POIS.Select(x => new POI()
            //{
            //    ID = x.ID,
            //    Name = x.Name,
            //    Longitude = x.Longitude,
            //    Latitude = x.Latitude
            //}).ToList();
            return pOIContext.POIS.ToList();
        }

        public POI GetById(Guid id)
        {
            //return pOIContext.POIS.Where(x => x.ID == id).Select(x => new POI()
            //{
            //    ID = x.ID,
            //    Name = x.Name,
            //    Longitude = x.Longitude,
            //    Latitude = x.Latitude
            //}).FirstOrDefault();
            return pOIContext.POIS.Find(id);
        }

        public void Update(POI entity)
        {
        //    var poi = new POI()
        //    {
        //        ID = entity.ID,
        //        Name = entity.Name,
        //        Longitude = entity.Longitude,
        //        Latitude = entity.Latitude
        //    };
            pOIContext.POIS.Update(entity);
            pOIContext.SaveChanges();
        }
    }
}
