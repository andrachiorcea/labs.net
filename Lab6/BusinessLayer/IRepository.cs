﻿using System;
using System.Collections.Generic;
using DataLayer;

namespace BusinessLayer
{
    public interface IRepository
    {
        POI GetById(Guid id);
        void Add(POI entity);
        void Delete(POI entity);
        void Update(POI entity);
        IReadOnlyList<POI> GetAll();
    }
}
