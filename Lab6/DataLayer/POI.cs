﻿using System;

namespace DataLayer
{
    public class POI
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }

        public POI()
        {

        }

        public POI(string name, string description, string lonngitude, string latitude)
        {
            this.Name = name;
            this.Description = description;
            this.Latitude = latitude;
            this.Longitude = lonngitude;
        }
    }
}
