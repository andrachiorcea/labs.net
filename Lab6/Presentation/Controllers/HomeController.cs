﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using BusinessLayer;
using DataLayer;
using Microsoft.AspNetCore.Mvc;
using Presentation.Models;
using PoiViewModel = Presentation.Models.PoiViewModel;


namespace Lab6.Controllers
{
    public class HomeController : Controller
    {
        private readonly IRepository _service;

        public HomeController(IRepository _service)
        {
            this._service = _service;
        }

        public IActionResult Index()
        {
            var pois = _service.GetAll();
            var POIVM = pois.Select(poi => new PoiViewModel()
                {
                    Name = poi.Name,
                    Longitude = poi.Longitude,
                    Description = poi.Description,
                    Latitude = poi.Latitude

                })
                .ToList();

            return View(POIVM);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(PoiViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var poiDTO = new POI()
            {
                ID = Guid.NewGuid(),
                Name = model.Name,
                Description = model.Description,
                Longitude = model.Longitude,
                Latitude = model.Latitude
            };
            _service.Add(poiDTO);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult Delete(PoiViewModel model)
        {
            var poiDTO = new POI()
            {
                Name = model.Name,
                Description = model.Description,
                Longitude = model.Longitude,
                Latitude = model.Latitude
            };
            _service.Delete(poiDTO);
            return RedirectToAction("Index");
        }

        public IActionResult Edit(Guid id)
        {
            var poi = _service.GetById(id);
            var PoiViewModel = new PoiViewModel
            {
                Name = poi.Name,
                Description = poi.Description,
                Longitude = poi.Longitude,
                Latitude = poi.Latitude
            };
            return View(PoiViewModel);
        }

        [HttpPost]
        public IActionResult Edit(PoiViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var poi = new POI()
            {
                Name = model.Name,
                Description = model.Description,
                Latitude = model.Latitude,
                Longitude = model.Longitude
            };
            _service.Update(poi);
            return RedirectToAction("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}
