﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ClassLibrary1;
using System;
using System.Linq;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest2
    {
        private ProductRepositoryQS _sut;
        private List<Product> _products = new List<Product>();

        [TestInitialize]
        public void TestInitializer()
        {
            DateTime s1 = new DateTime(2018, 5, 1, 8, 30, 52);
            DateTime e1 = new DateTime(2019, 5, 1, 8, 30, 52);
            Product p1 = new Product(1, "ceva", "descriere", s1, e1, 100);
            _products.Add(p1);

            DateTime s2 = new DateTime(2018, 5, 1, 8, 30, 52);
            DateTime e2 = new DateTime(2019, 5, 1, 8, 30, 52);
            Product p2 = new Product(1, "n2", "descriere", s2, e2, 100);
            _products.Add(p2);

            DateTime s3 = new DateTime(2020, 5, 1, 8, 30, 52);
            DateTime e3 = new DateTime(2024, 5, 1, 8, 30, 52);
            Product p3 = new Product(1, "n3", "descriere", s3, e3, 100);
            _products.Add(p3);

            DateTime s4 = new DateTime(2018, 5, 1, 8, 30, 52);
            DateTime e4 = new DateTime(2019, 5, 1, 8, 30, 52);
            Product p4 = new Product(1, "n4", "descriere", s4, e4, 100);
            _products.Add(p4);

            DateTime s5 = new DateTime(2018, 5, 1, 8, 30, 52);
            DateTime e5 = new DateTime(2019, 5, 1, 8, 30, 52);
            Product p5 = new Product(1, "n5", "descriere", s5, e5, 100);
            _products.Add(p5);

            DateTime s6 = new DateTime(2015, 5, 1, 8, 30, 52);
            DateTime e6 = new DateTime(2017, 5, 1, 8, 30, 52);
            Product p6 = new Product(1, "n6", "descriere", s6, e6, 100);
            _products.Add(p6);

            DateTime s7 = new DateTime(2018, 5, 1, 8, 30, 52);
            DateTime e7 = new DateTime(2019, 5, 1, 8, 30, 52);
            Product p7 = new Product(1, "n7", "descriere", s7, e7, 100);
            _products.Add(p7);

            DateTime s8 = new DateTime(2018, 5, 1, 8, 30, 52);
            DateTime e8 = new DateTime(2019, 5, 1, 8, 30, 52);
            Product p8 = new Product(1, "n8", "descriere", s8, e8, 100);
            _products.Add(p8);

            DateTime s9 = new DateTime(2018, 5, 1, 8, 30, 52);
            DateTime e9 = new DateTime(2019, 5, 1, 8, 30, 52);
            Product p9 = new Product(1, "n9", "descriere", s9, e9, 100);
            _products.Add(p9);

            DateTime s10 = new DateTime(2018, 5, 1, 8, 30, 52);
            DateTime e10 = new DateTime(2019, 5, 1, 8, 30, 52);
            Product p10 = new Product(1, "n10", "descriere", s10, e10, 100);
            _products.Add(p10);

            _sut = new ProductRepositoryQS(_products);
        }

        [TestCleanup]
        public void TestCleanup()
        {
            _sut = null;
        }

        [TestMethod]
        public void WhenCallingRetrieveActiveProducts_ResultLengthShouldBe_Nine()
        {
            var rez = _sut.RetrieveActiveProducts();
            Assert.AreEqual(9, rez.Count());
        }

        [TestMethod]
        public void WhenCallingRetrieveInactiveProducts_ResultShouldBe_One()
        {
            var rez = _sut.RetrieveInactiveProducts();
            Assert.AreEqual(rez.Count(), 1);
        }

        [TestMethod]
        public void WhenCallingRetrieveAll_CriteriaDate_ResultLengthShouldBe_1()
        {
            var rez = _sut.RetrieveAll(new DateTime(2020, 5, 1, 8, 30, 52), new DateTime(2024, 5, 1, 8, 30, 52));
            Assert.AreEqual(rez.Count(), 1);
        }

        [TestMethod]
        public void WhenCallingRetrieveAllOrderByPriceDescending_ResultShouldBe_True()
        {
            var rez = _sut.RetriveAllOrderByPriceDescending();
            _products.Sort((x, y) => -1 * x.Price.CompareTo(y.Price));
            CollectionAssert.AreEqual(_products, rez.ToList());
        }

        [TestMethod]
        public void WhenCallingRetrieveAllOrderByPriceAscending_ResultShouldBe_True()
        {
            var rez = _sut.RetriveAllOrderByPriceAscending();
            _products.Sort((x, y) => x.Price.CompareTo(y.Price));
            CollectionAssert.AreEqual(_products, rez.ToList());
        }

        [TestMethod]
        public void WhenCallingRetrieveAll_CriteriaIsName_ResultShouldBe_One()
        {
            var rez = _sut.RetrieveAll("ceva");
            Assert.AreEqual(rez.Count(), 1);
        }
    }
}
