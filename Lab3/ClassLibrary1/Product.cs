﻿using System;

namespace ClassLibrary1
{
    public class Product
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public string Description { get; private set; }
        public DateTime StartDate { get; private set; }
        public DateTime EndDate { get; private set; }
        public double Price { get; private set; }
        private const double VAT = 19;

        public Product(int id, string name, string description, DateTime startDate, DateTime endDate, double price)
        {
            this.Id = id;
            this.Name = name;
            this.Description = description;
            this.StartDate = startDate;
            this.EndDate = endDate;
            this.Price = price;
        }

        public bool IsValid()
        {
            return this.StartDate < this.EndDate & this.EndDate >= DateTime.Now;
        }

        public double ComputeVat()
        {
            return this.Price * VAT / 100;
        }
    }
}
