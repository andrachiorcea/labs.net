﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ClassLibrary1
{
    public class ProductRepositoryMS
    {

        private List<Product> _products = new List<Product>();

        public ProductRepositoryMS(List<Product> products)
        {
            _products = products;
        }

        public IEnumerable<Product> RetrieveActiveProducts()
        {
            IEnumerable<Product> products = this._products.Where(p => p.IsValid() == true);
            return products;
        }

        public IEnumerable<Product> RetrieveInactiveProducts()
        {
            IEnumerable<Product> products = this._products.Where(p => p.IsValid() == false);
            return products;
        }

        public IEnumerable<Product> RetrieveAllOrderByPriceDescending()
        {
            IEnumerable<Product> products = this._products.OrderByDescending(p => p.Price);
            return products;
        }

        public IEnumerable<Product> RetrieveAllOrderByPriceAscending()
        {
            IEnumerable<Product> products = this._products.OrderBy(p => p.Price);
            return products;
        }

        public IEnumerable<Product> RetrieveAll(string name)
        {
            IEnumerable<Product> products = this._products.Where(p => p.Name == name);
            return products;
        }

        public IEnumerable<Product> RetrieveAll(DateTime startDate, DateTime endDate)
        {
            IEnumerable<Product> products = this._products.Where(p => p.StartDate == startDate && p.EndDate == endDate);
            return products;
        }
    }
}
