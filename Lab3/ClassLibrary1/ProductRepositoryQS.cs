﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ClassLibrary1
{
    public class ProductRepositoryQS
    {

        private List<Product> _products = new List<Product>();

        public ProductRepositoryQS(List<Product> products)
        {
            _products = products;
        }

        public IEnumerable<Product> RetrieveActiveProducts()
        {
             IEnumerable<Product> products = from p in this._products where p.IsValid() == true select p;
             return products;
        }

        public IEnumerable<Product> RetrieveInactiveProducts()
        {
            IEnumerable<Product> products = from p in this._products where p.IsValid() == false select p;
            return products;
        }

        public IEnumerable<Product> RetriveAllOrderByPriceDescending()
        {
            IEnumerable<Product> products = from p in this._products orderby p.Price descending select p;
            return products;
        }

        public IEnumerable<Product> RetriveAllOrderByPriceAscending()
        {
            IEnumerable<Product> products = from p in this._products orderby p.Price ascending select p;
            return products;
        }

        public IEnumerable<Product> RetrieveAll(string name)
        {
            IEnumerable<Product> products = from p in this._products where p.Name == name select p;
            return products;
        }

        public IEnumerable<Product> RetrieveAll(DateTime startDate, DateTime endDate)
        {
            IEnumerable<Product> products = from p in this._products where p.StartDate== startDate && p.EndDate == endDate select p;
            return products;
        }
    }
}
