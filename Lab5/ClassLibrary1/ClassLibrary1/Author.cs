﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Vanguard;

namespace ClassLibrary1
{
    public class Author
    {
        public Guid AuthorId { get; private set; }
        public ICollection<Book> Books { get; private set; } 

        [Required]
        [MinLength(50)]
        [MaxLength(100)]
        public string FirstName { get; private set; }

        [Required]
        [MaxLength(150)]
        public string LastName { get; private set; }

        public Author(string firstName, string lastName)
        {
            this.AuthorId = new Guid();
            this.FirstName = firstName;
            this.LastName = lastName;
            Books = new List<Book>();
        }

        public Author()
        {

        }

        public void AttachBook(Book book)
        {
            Guard.ArgumentNotNull(book, nameof(book));
            Books.Add(book);
        }

        public Book DetachBook(Guid id)
        {
            var book = Books.FirstOrDefault(b => b.bookId == id);
            Books.Remove(book);
            return book;
        }
    }
}
