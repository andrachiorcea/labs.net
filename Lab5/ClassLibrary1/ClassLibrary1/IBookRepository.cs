﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1
{
    public interface IBookRepository: IRepository<Book>
    {
        void CreateBook(Book book);
        void RemoveBookById(int id);
    }
}
