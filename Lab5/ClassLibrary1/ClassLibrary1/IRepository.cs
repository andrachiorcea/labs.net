using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1
{
    public interface IRepository<TEntity> where TEntity : class
    {
        TEntity GetById(int id);
        IEnumerable<TEntity> GetAll();
    }
}
