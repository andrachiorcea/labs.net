using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace ClassLibrary1
{
    public class BookRepository : Repository<Book>, IBookRepository
    {
        public ApplicationContext context;

        public BookRepository(ApplicationContext context)
        {
            this.context = context;
        }

        public void CreateBook(Book book)
        {
            context.Set<Book>().Add(book);
        }

        public void RemoveBookById(int id)
        {
            Book toRemove = this.GetById(id);
            context.Set<Book>().Remove(toRemove);
        }
    }
}
