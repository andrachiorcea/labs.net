﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace ClassLibrary1
{
    public class Book
    {
        public int BookId { get; private set; }

        [ForeignKey("AuthorId")] public Author author { get; private set; }

        [Required]
        [MinLength(50)]
        [MaxLength(100)]
        public string Title { get; private set; }

        public Book(string title)
        {
            this.BookId = 1;
            this.Title = title;
        }

        public Book()
        {

        }
    }
}
