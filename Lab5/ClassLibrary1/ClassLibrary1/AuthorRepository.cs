using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace ClassLibrary1
{
    public class AuthorRepository : Repository<Author>, IAuthorRepository
    {
        public ApplicationContext context;

        public AuthorRepository(ApplicationContext context)
        {
            this.context = context;
        }

        public void CreateAuthor(Author Author)
        {
            Context.Set<Author>().Add(Author);
        }

        public void RemoveAuthorById(int id)
        {
            Author toRemove = this.GetById(id);
            Context.Set<Author>().Remove(toRemove);
        }
    }
}
