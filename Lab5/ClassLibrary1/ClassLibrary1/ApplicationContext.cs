using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Text;


namespace ClassLibrary1
{
    public class ApplicationContext : DbContext
    {
        public virtual DbSet<Author> Authors { get; set; }

        public ApplicationContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {

                optionsBuilder.UseSqlServer(
                    @"Server=(localdb)\mssqllocaldb;Database=MyData;Trusted_Connection=True;");

            }

        }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<Book>()
        //        .HasOne(p => p.author)
        //        .WithMany(b => b.books);

        //    modelBuilder.Entity<Book>().Property(book => book.Title).IsRequired().HasMaxLength(100);

        //    modelBuilder.Entity<Author>().Property(author => author.FirstName).IsRequired().HasMaxLength(50);
        //    modelBuilder.Entity<Author>().Property(author => author.LastName).IsRequired().HasMaxLength(70);
        //}
    }
}
