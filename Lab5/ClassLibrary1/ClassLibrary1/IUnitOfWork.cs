﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1
{
    public interface IUnitOfWork : IDisposable
    {
        void CreateAuthor(Author author);
        void DeleteAuthor(Guid id);
        void CreateBook(Guid authorId, Book book);
        void DeleteBook(Guid authorId, Guid bookId);
    }
}
