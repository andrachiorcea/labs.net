﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1
{
    public interface IAuthorRepository: IRepository<Author>
    {
        void CreateAuthor(Author author);
        void RemoveAuthorById(int id);
    }
}
