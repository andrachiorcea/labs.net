﻿using System;
using ClassLibrary1;

namespace ConsoleApp1
{
    class Program
    {
        private static UnitOfWork unitOfWork = new UnitOfWork(new ApplicationContext());

        static void Main(string[] args)
        {
            var unitOfWork = new UnitOfWork(new ApplicationContext());

            var book = new Book("Carte");
            unitOfWork.Books.CreateBook(book);
            unitOfWork.Complete();
            //Book b1 = unitOfWork.BookRepository.GetById('');

            //unitOfWork.Save();
            //Book Book = unitOfWork.BookRepository.GetByID(id);
            //// ...
            //unitOfWork.BookRepository.Update(Book);
            //unitOfWork.Save();
            //// ...
            //var departmentsQuery = unitOfWork.DepartmentRepository.Get(
            //    orderBy: q => q.OrderBy(d => d.Name));
            //// ...
            //Book Book = unitOfWork.BookRepository.GetByID(id);
            //// ...
            //unitOfWork.BookRepository.Delete(id);
            //unitOfWork.Save();
            //// ...
            unitOfWork.Dispose();
        }
    }
}


