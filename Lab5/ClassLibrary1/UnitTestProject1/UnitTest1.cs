//using Microsoft.EntityFrameworkCore;
//using Microsoft.VisualStudio.TestTools.UnitTesting;

//namespace UnitTestProject1
//{
//    [TestClass]
//    public class UnitTest1
//    {
//        [TestClass]
//        public void Add_TestClassObjectPassed_ProperMethodCalled()
//        {
//            // Arrange
//            var testObject = new TestClass();

//            var context = new Mock<Dbcontext>();
//            var dbSetMock = new Mock<DbSet<TestClass>>();
//            context.Setup(x => x.Set<TestClass>()).Returns(dbSetMock.Object);
//            dbSetMock.Setup(x => x.Add(It.IsAny<TestClass>())).Returns(testObject);

//            // Act
//            var repository = new Repository<TestClass>(context.Object);
//            repository.Add(testObject);

//            //Assert
//            context.Verify(x => x.Set<TestClass>());
//            dbSetMock.Verify(x => x.Add(It.Is<TestClass>(y => y == testObject)));
//        }
//    }
//}
