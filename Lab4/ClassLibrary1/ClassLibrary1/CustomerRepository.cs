﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassLibrary1
{
    public class CustomerRepository : IRepository<Customer>
    {
        private readonly DBManager _dbManagerCustomercontext;

        public CustomerRepository()
        {
            _dbManagerCustomercontext = new DBManager();
        }

        public void Create(Customer entity)
        {
            _dbManagerCustomercontext.Customers.Add(entity);
            _dbManagerCustomercontext.SaveChanges();
        }

        public void Delete(Customer entity)
        {
            _dbManagerCustomercontext.Customers.Remove(entity);
            _dbManagerCustomercontext.SaveChanges();
        }

        public IEnumerable<Customer> GetAll()
        {
            return _dbManagerCustomercontext.Customers;
        }

        public Customer GetById(Guid id)
        {
            return _dbManagerCustomercontext.Customers.Where(x => x.Id == id).FirstOrDefault();
        }

        public void Update(Customer entity)
        {
            _dbManagerCustomercontext.Set<Customer>().Update(entity);
            _dbManagerCustomercontext.SaveChanges();
        }

        public Customer GetByEmail(string email)
        {
            return _dbManagerCustomercontext.Customers.Where(x => x.Email == email).FirstOrDefault();
        }
    }
}
