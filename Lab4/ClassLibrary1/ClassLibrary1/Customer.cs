﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ClassLibrary1
{
    public class Customer
    {
        public Guid Id { get; private set; }

        [Required]
        [StringLength(100)]
        public string Name { get; private set; }

        [Required]
        [StringLength(300)]
        public string Address { get; private set; }

        [Required]
        [RegularExpression(("(07)([0-9]{8})"))]
        public string PhoneNumber { get; private set; }

        [Required]
        [RegularExpression("[(A-Za-z0-9)]*@[(A-Za-z0-9)]*.[(A-Za-z0-9)]*")]
        public string Email { get; private set; }

        public Customer(string name, string address, string phoneNumber, string email)
        {
            this.Id = Guid.NewGuid();
            this.Name = name;
            this.Address = address;
            this.PhoneNumber = phoneNumber;
            this.Email = email;
        }

        public Customer()
        {

        }
    }
}
