﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Text;

namespace ClassLibrary1
{
    public class DBManager : Dbcontext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Customer> Customers { get; set; }

        public DBManager()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbcontextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=MyData;Trusted_Connection=True;");
            }

        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>().Property(customer => customer.Name).IsRequired().HasMaxLength(100);
            modelBuilder.Entity<Customer>().Property(customer => customer.Address).IsRequired().HasMaxLength(300);
            modelBuilder.Entity<Customer>().Property(customer => customer.PhoneNumber).IsRequired();
            modelBuilder.Entity<Customer>().Property(customer => customer.Email).IsRequired();

            modelBuilder.Entity<Employee>().Property(employee => employee.FirstName).IsRequired().HasMaxLength(50);
            modelBuilder.Entity<Employee>().Property(employee => employee.LastName).IsRequired().HasMaxLength(70);
            modelBuilder.Entity<Employee>().Property(employee => employee.StartDate).IsRequired();
        }
    }
}
