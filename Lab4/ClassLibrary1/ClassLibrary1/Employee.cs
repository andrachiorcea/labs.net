﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ClassLibrary1
{
    public class Employee
    {
        public Guid Id { get; private set; }

        [Required]
        [StringLength(50)]
        public string FirstName { get; private set; }

        [Required]
        [StringLength(70)]
        public string LastName { get; private set; }

        [Required]
        public DateTime StartDate { get; private set; }

        public DateTime? EndDate { get; private set; }
        public double Salary { get; private set; }

        public Employee()
        {

        }

        public Employee(string firstName, string lastName, DateTime startDate, DateTime endDate, double salary)
        {
            this.Id = Guid.NewGuid();
            this.FirstName = firstName;
            this.LastName = lastName;
            this.StartDate = startDate;
            this.EndDate = endDate;
            this.Salary = salary;
        }

        public string GetFullName()
        {
            return this.FirstName + this.LastName;
        }

        public bool IsActive()
        {
            if (DateTime.Now >= this.StartDate && DateTime.Now <= this.EndDate && this.StartDate < this.EndDate)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
   
}
