﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibrary1
{
    public interface IRepository<T> where T : class
    {
        void Create(T entity);
        void Update(T entity);
        void Delete(T entity);
        T GetById(Guid id);
        IEnumerable<T> GetAll();
    }
}
