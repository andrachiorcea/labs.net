﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassLibrary1
{
    public class EmployeeRepository : IRepository<Employee>
    {
        private readonly DBManager _dbManagerEmployeeContext;

        public EmployeeRepository()
        {
            _dbManagerEmployeeContext = new DBManager();
        }

        public void Create(Employee entity)
        {
            _dbManagerEmployeeContext.Employees.Add(entity);
            _dbManagerEmployeeContext.SaveChanges();
        }

        public void Delete(Employee entity)
        {
            _dbManagerEmployeeContext.Employees.Remove(entity);
            _dbManagerEmployeeContext.SaveChanges();
        }

        public IEnumerable<Employee> GetAll()
        {
            return _dbManagerEmployeeContext.Employees;
        }

        public Employee GetById(Guid id)
        {
            return _dbManagerEmployeeContext.Employees.Where(x => x.Id == id).FirstOrDefault();
        }

        public void Update(Employee entity)
        {
            _dbManagerEmployeeContext.Set<Employee>().Update(entity);
            _dbManagerEmployeeContext.SaveChanges();
        }

        public IEnumerable<Employee> GetEmployeesBySalary(double salary)
        {
            return _dbManagerEmployeeContext.Employees.Where(x => x.Salary == salary).ToList();
        }
    }
}
