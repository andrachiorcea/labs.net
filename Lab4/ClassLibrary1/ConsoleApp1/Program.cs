﻿using System;
using ClassLibrary1;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //var employeeRepository = new EmployeeRepository();
            var customerRepository = new CustomerRepository();
            
            //var employee = new Employee("Andra", "Chiorcea", new DateTime(2018, 11, 10), new DateTime(2019, 11, 10), 100000);
            //employeeRepository.Create(employee);
            //Console.WriteLine(employeeRepository.GetById(Guid.NewGuid()).ToString());
            //Console.ReadKey();

            var customer = new Customer("ilinca", "adresa", "0712345678", "ilinca@gmail.com");
            customerRepository.Create(customer);
            Console.WriteLine(customerRepository.GetByEmail("ilinca@gmail.com").ToString());
            Console.ReadKey();

        }
    }
}
